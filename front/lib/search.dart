import 'dart:ui';

import 'package:flutter/material.dart';

List filesList = [];

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {

  //CREATE DATA FOR EXAMPLE
  List _buildItems(int count) {
    List<Widget> listItems = [];

    for (int i = 0; i < count; i++) {
      listItems.add(
        Card(
          elevation: 5.0,
          child: Center(
            child: Text(
              'Image ${(i+1).toString()}',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 22.0),
            ),
          ),
        ),
      );
    }
    return listItems;
  }

  @override
  Widget build(BuildContext context) {
    //TO REMOVE BY GETALLIMAGE THAT RETURN LIST<IMAGE>
    List<Widget> listWidget = _buildItems(25);
    return MaterialApp(
      home: Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top:30, left:10, right:10, bottom:10),
            child: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red, width: 20.0),
                ),
                labelText: 'Search',
                hintText: 'Search photos'),
            ),
          ),
          Expanded(
            child: GridView.count(
            crossAxisCount: 3,
            crossAxisSpacing: 5,
            mainAxisSpacing: 5,
            padding: EdgeInsets.all(10.0),
            children: listWidget,
            ),
          )
        ],
      ),
      ));
  }
}