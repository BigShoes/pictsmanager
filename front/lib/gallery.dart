import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:front/singleView.dart';

List filesList = [];
var _USERNAME = "username_123";

class Gallery extends StatefulWidget {
  @override
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  //CREATE DATA FOR EXAMPLE
  List _buildItems(int count) {
    List<Widget> listItems = [];

    for (int i = 0; i < count; i++) {
      listItems.add(
        Card(
          elevation: 5.0,
          child: Center(
            child: Text(
              'Image ${(i+1).toString()}',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 22.0),
            ),
          ),
        ),
      );
    }
    return listItems;
  }

  @override
  Widget build(BuildContext context) {
    //TO REMOVE BY GETALLIMAGE THAT RETURN LIST<IMAGE>
    List<Widget> listWidget = _buildItems(25);
    return MaterialApp(
      home: Scaffold(
      body: Column(
        children: <Widget>[
          Row(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                height: 150,
                margin: EdgeInsets.all(10),
                child: CircleAvatar(
                  radius: 75,
                  backgroundImage: AssetImage("asset/images/profilepic.jpeg"),
                ),
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  border: new Border.all(
                    color: Colors.grey,
                    width: 4.0,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  '${_USERNAME}',
                  style: const TextStyle(
                    fontSize: 35,
                  ),
                ),
                height: 150,
              )
            ],
          ),
          //ADD OPTION TAB
          Expanded(
            child: GridView.count(
            crossAxisCount: 3,
            crossAxisSpacing: 5,
            mainAxisSpacing: 5,
            padding: EdgeInsets.all(10.0),
            children: listWidget,
            ),
          )
        ],
      ),
    ),
    );
  }
}

class GridItem extends StatelessWidget {
  int id = 1;

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap:() => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SingleView()),
      ),
    );
  }
}