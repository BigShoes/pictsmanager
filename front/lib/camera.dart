import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io';

class Camera extends StatefulWidget {
  final loadingWidget;
  Camera(this.loadingWidget);
  _CameraState createState() => _CameraState();
}

class _CameraState extends State<Camera> with WidgetsBindingObserver {
  List<CameraDescription> _cameras;
  CameraController _controller;
  int _selected = 0;
  bool _flashMode = false;

  @override
  void initState() {
    super.initState();
    setupCamera();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.addObserver(this);
    _controller?.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (_controller == null || !_controller.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      _controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      setupCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_controller == null) {
      if (widget.loadingWidget != null) {
        return widget.loadingWidget;
      } else {
        return Container(
          color: Colors.black,
        );
      }
    } else {
      final size = MediaQuery.of(context).size;
      final deviceRatio = size.width / size.height;
      return new Scaffold(
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Center(
              child:Transform.scale(
                scale: _controller.value.aspectRatio/deviceRatio,
                child: new AspectRatio(
                  aspectRatio: _controller.value.aspectRatio,
                  child: new CameraPreview(_controller),
                ),
              ),
            ),
            Row(
              children: <Widget>[
                IconButton(
                  icon: _flashMode
                  ?Icon(Icons.flash_on_outlined)
                  :Icon(Icons.flash_off_outlined),
                  iconSize: 50.0,
                  onPressed: () {
                    setState(() {
                      _flashMode = !_flashMode;
                    });
                  },
                ),
                IconButton(
                  icon: const Icon(Icons.circle_outlined),
                  iconSize: 125.0,
                  onPressed: () => takePicture(),
                ),
                IconButton(
                  icon: Icon(Icons.switch_camera_outlined),
                  iconSize: 50.0,
                  onPressed: () => toggleCamera(),
                ),
              ],
              crossAxisAlignment: CrossAxisAlignment.center,
              verticalDirection: VerticalDirection.down,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              ),
          ],
        )
      );
    }
  }

  Future<void> setupCamera() async {
    await [
      Permission.camera,
    ].request();
    _cameras = await availableCameras();
    var controller = await selectCamera();
    setState(() => _controller = controller);
  }

  selectCamera() async {
    var controller = CameraController(_cameras[_selected], ResolutionPreset.low);
    await controller.initialize();
    return controller;
  }

  Future<void> takePicture() async {
    try {
      final Directory directory = await getApplicationDocumentsDirectory();
      Directory _imagesFolder = Directory('${directory.path}/gallery');
      if (!_imagesFolder.existsSync()) {
        _imagesFolder.createSync();
      }

      //PROBABLY A BETTER WAY TO DO IT ?
      _controller.setFlashMode(_flashMode? FlashMode.torch : FlashMode.off);
      XFile image = await _controller.takePicture();
      //TO DELETE
      String userTag = "AAAAAA";
      await PushAsyncPhoto(image, userTag);
      _controller.setFlashMode(FlashMode.off);
    } catch(e) {
      print(e);
    }
  }

  toggleCamera() async {
    int newSelected = (_selected + 1) % _cameras.length;
    _selected = newSelected;

    var controller = await selectCamera();
    setState(() => _controller = controller);
  }

  PushAsyncPhoto(XFile, string) async {
  //CREATE REQUEST OR CALL METHOD THAT CREATE REQUEST
  //PUSH REQUEST
  }
}