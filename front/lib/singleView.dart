import 'package:flutter/material.dart';

class SingleView extends StatefulWidget {
  @override
  _SingleViewState createState() => _SingleViewState();
}

class _SingleViewState extends State<SingleView> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SizedBox(
        width: double.infinity,
        height: 500,
        child: Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          elevation: 5.0,
          margin: EdgeInsets.all(20),
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.white70, width: 1),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Center(
            child: Text(
              'Image 1',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 22.0),
            ),
          ),
        ),
      ),
    );
  }
}
