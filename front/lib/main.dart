import 'package:flutter/material.dart';

import 'camera.dart';
import 'gallery.dart';
import 'login.dart';
import 'signup.dart';
import 'search.dart';
import 'singleView.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/gallery',
      routes: {
        '/login': (BuildContext ctx) => Login(),
        '/signup': (BuildContext ctx) => SignUp(),
        '/search': (BuildContext ctx) => Search(),
        '/singleView': (BuildContext ctx) => SingleView(),
        '/gallery': (BuildContext ctx) => Gallery(),
        '/camera': (BuildContext ctx) => Camera(null),
      },
    );
  }
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int currentTabIndex = 0;
  List<Widget> tabs = [
    Gallery(),
    Camera(null),
    Search(),
    Login(),
    SignUp(),
    SingleView(),
  ];

  onTapped(int index) {
    setState(() {
      currentTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: tabs[currentTabIndex],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTapped,
          currentIndex: currentTabIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              label: "Home"
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.camera_alt_outlined),
              label: "Camera"
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label: "Search"
            ),
          ],
        ),
      )
    );
  }
}