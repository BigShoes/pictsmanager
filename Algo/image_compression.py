""" This file is a wrapper for the kmeans algorithm. 
Refer to Test_algo.ipynb to a have a complete exemple.
"""

import cv2
import matplotlib.pyplot as plt
from time import time
from PIL import Image


def actualsize(input_obj):
    """Get size of an image"""
    return os.stat(f'{input_obj}.png').st_size


def get_time(f):
    def new_fn(*args, **kwargs):
        
        t0 = time()
        result = f(*args, **kwargs)
        print(f'Done in: {time() - t0}')
        
        return result, time() - t0
    
    return new_fn


class ImageCompression():
    """ Implement a wrapper for image compression."""

    def __init__(self, image):
        
        self.image = image
        self.current_dim = self.image.shape
        self.image_flat = self.image.reshape(-1, self.image.shape[-1])
       
    @get_time
    def get_image_resized(self, width, height, algo=cv2.INTER_AREA):
        """Used to resize an image."""

        self.image_resized = cv2.resize(self.image, (width, height), interpolation=algo)
        self.current_dim = self.image_resized.shape
        self.image_flat = self.image_resized.reshape(-1, self.image_resized.shape[-1])
    
    @get_time
    def get_image_clustered(self, cluster_fn, n_clusters=100, batch_size=1000, max_iter=100):
        """Used to get the clustered image."""

        cluster = cluster_fn(n_clusters=n_clusters, batch_size=batch_size, max_iter=max_iter)
        self.image_clustered = cluster.fit_transform(self.image_flat)
        self.image_clustered = self.image_clustered.reshape(self.current_dim)
                
        return cluster

    def save_image(self, image, name):
        """ Save the image with a name."""

        im = Image.fromarray(image)
        im.save(f"{name}.png")
    
    def show_images(self, images, names):
        """ Show the images before clustering and after."""

        for idx, image in enumerate(images):
            plt.subplot(1, len(images), idx+1)
            plt.imshow(image)
            self.save_image(image, names[idx])
            plt.title(f'Image take {actualsize(names[idx])} bytes')