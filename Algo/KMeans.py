import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import euclidean_distances

class KMeans(TransformerMixin, BaseEstimator):
    """ Kmeans algorithm.

    Parameters
    ----------

    Attributes
    ----------
    """
    def __init__(self, n_clusters=8, *,
                 batch_size=1000,
                 max_iter=100):
        
        self.n_clusters = n_clusters
        self.batch_size = batch_size
        self.max_iter = max_iter

    def euclidean_distances(self, X, y):
        """ Compute the euclidean distances between each point of X and y.
        """
        X = X.astype('float64')
        y = y.astype('float64')
        X = np.expand_dims(X, axis=-1)
        y = np.expand_dims(y, axis=0)
        carre = np.square(X - y, dtype='float64')
        s = np.sum(carre, axis=1)
        return s

    def inertia(self, X, clusters):
        """Calculate inertia.
        """

        distances = euclidean_distances(X, clusters)
        return distances.min(axis=1).sum()
    
    def init_random(self, X, n_clusters):
        """Initialize centroids for kmean clusterisation.
        """

        coordinates_clusters = X[np.random.choice(X.shape[0], n_clusters, replace=False)]
        return coordinates_clusters

    def kmean_algorithm(self, image, n_clusters, max_iter, batch_size):
        """Implementation of our kmean algorithm.
        """

        coordinates_clusters = self.init_random(image, n_clusters) # Init centroids
        # temporary copy of centroids in order tot try inertia before applying changement
        coordinates_clusters_tmp = np.zeros(coordinates_clusters.shape) 
        clusters_counts = np.zeros((coordinates_clusters.shape[0], ))
        history = 0
    
        for t in range(max_iter):
            
            M = image[np.random.choice(image.shape[0], batch_size, replace=False)] # We sample pixels 
            # We calculate the nearest centroids for each pixels
            nearest_cluster = np.argmin(euclidean_distances(M, coordinates_clusters), axis=1) 

            for idx in range(n_clusters):
                
                if len(M[nearest_cluster == idx]) == 0:
                    new_cluster = coordinates_clusters[idx]
                else:
                    new_cluster = M[nearest_cluster == idx].mean(axis=0)
                                
                coordinates_clusters_tmp[idx] = new_cluster
                
            inertie = self.inertia(M, coordinates_clusters_tmp)
            
            # If inertia is increasing we stop iterations
            if inertie < history and t > 0:
                break

            coordinates_clusters[:, :] = coordinates_clusters_tmp
            history = inertie
            
        return coordinates_clusters, history

    def fit(self, X, y=None):
        """Method used to to train our kmean algorithm
        """
        # Input validation
        X = check_array(X, accept_sparse=True)
        
        kmeans = [self.kmean_algorithm(X, self.n_clusters, self.max_iter, self.batch_size)  for _ in range(10)]
        kmeans = sorted(kmeans, key=lambda x: x[1]) # We sort in function of its inertia
        self.cluster_centers_ = kmeans[0][0]

        # Return the transformer
        return self

    def transform(self, X):
        """ Method used to transform an image into its clestered image.
        """
        # Check is fit had been called
        check_is_fitted(self, 'cluster_centers_')

        # Input validation
        X = check_array(X, accept_sparse=True)

        image_cluster = np.argmin(euclidean_distances(X, self.cluster_centers_), axis=1).T
        return np.apply_along_axis(lambda x: self.cluster_centers_[x], 0, image_cluster)

    def fit_transform(self, X):
        """ Implement the fit and transform method in one.
        """
        self.fit(X)
        return self.transform(X)